//
//  main.cpp
//  Dominos
//
//  Created by Nuno Maia on 3/16/13.
//  Copyright (c) 2013 Nuno Maia. All rights reserved.
//

#include <iostream>
#include <vector>
#include <list>
#include <stack>

using namespace std;

struct vertex {
    int id;
    int index;
    int lowlink;
    int component;
    list<vertex *> *adjacents;
    
    vertex(int id) : id(id), index(-1), lowlink(-1), component(-1), adjacents(new list<vertex *>()) {
        
    }
    
    ~vertex() {
        delete adjacents;
    }

};

bool stack_has_vertex(list<vertex *> *s, vertex* v) {
    list<vertex *>::iterator it;
    for (it = s->begin(); it != s->end(); it++) {
        if ((*it)->id == v->id) {
            return true;
        }
    }
    return false;
}

int gindex;
int cindex;
int sccs;

void strong_connect(list<vertex *> *s, vertex *v) {
    v->index = gindex;
    v->lowlink = gindex;
    gindex++;
    s->push_front(v);
    
    list<vertex *>::iterator adj;
    for (adj = v->adjacents->begin(); adj != v->adjacents->end(); adj++) {
        if ((*adj)->index == -1) {
            strong_connect(s, *adj);
            v->lowlink = min((*adj)->lowlink, v->lowlink);
        } else if (stack_has_vertex(s, *adj)) {
            v->lowlink = min(v->lowlink, (*adj)->index);
        }
    }
    
    if (v->index == v->lowlink) {
        vertex *w = s->front();
        
        while (w->index != v->index) {
            s->pop_front();
            w->component = cindex;
            w = s->front();
        }
        
        w->component = cindex;
        s->pop_front();
        cindex++;
        
        sccs++;
    }
    
}

int main(int argc, const char * argv[])
{
    int test_cases, tiles, lines;
    vector<vertex *> vertexes;
    list<vertex *> *stack = new list<vertex *>();
    list<int> results;
    int result = 0;
    
    cin >> test_cases;
    for (int i = 0; i < test_cases; i++) {
        cin >> tiles >> lines;
        for (int j = 0; j < tiles; j++) {
            vertexes.push_back(new vertex(j + 1));
        }

        for (int j = 0; j < lines; j++) {
            int x, y;
            
            cin >> x >> y;
            vertexes[x - 1]->adjacents->push_back(vertexes[y - 1]);
        }
        
        
        gindex = 0;
        cindex = 0;
        sccs = 0;
        vector<vertex *>::iterator it;
        for (it = vertexes.begin(); it != vertexes.end(); it++) {
            if ((*it)->index == -1) {
                strong_connect(stack, (*it));
            }
        }
        
        vector<bool> check_table(sccs, false);
        
        vector<vertex *>::iterator vv;
        for (vv = vertexes.begin(); vv != vertexes.end(); vv++) {
            list<vertex *>::iterator adj;
            for (adj = (*vv)->adjacents->begin(); adj != (*vv)->adjacents->end(); adj++) {
                if ((*vv)->component != (*adj)->component) {
                    check_table[(*adj)->component] = true;
                }
            }
        }
        
        
        if (sccs == 1) {
            result = 1;
        } else {
            vector<bool>::iterator ct;
            for (ct = check_table.begin(); ct != check_table.end(); ct++) {
                if ((*ct) == false) {
                    result++;
                }
            }
            
            if (result == 0) {
                result = 1;
            }
        }
        
        results.push_back(result);
        result = 0;
        vertexes.clear();
        stack->clear();
    }
    
    list<int>::iterator r;
    for (r = results.begin(); r != results.end(); r++) {
        cout << *r << endl;
    }
    
    delete stack;
    
}
